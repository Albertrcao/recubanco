<?php
require_once ("../Helpers/DBManager.php");

function transfer($origin, $destiny, $amount){
    $manager = new DBManager();
    try {
        $lastId=getLastIdMovement()+1;
        $sql = "INSERT INTO movimientos (id,fecha,cantidad,id_origen,id_destino) VALUES (:id,now(),:cantidad,:id_origen,:id_destino)";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $lastId);;
        $stmt->bindParam(':cantidad', $amount);
        $stmt->bindParam(':id_origen', $origin);
        $stmt->bindParam(':id_destino', $destiny);
        $stmt->execute();
        $sql = "UPDATE cuenta SET saldo = saldo - $amount WHERE id=:origen";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':origen', $origin);
        $stmt->execute();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getLastIdMovement(){
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM movimientos ORDER BY id DESC limit 1";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (sizeof($rt)>0){
            return $rt[0]['id'];
        }else{
            return 0;
        }
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}

function getMovements($account)
{
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM movimientos WHERE id_origen=:cuenta OR id_destino=:cuenta";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':cuenta', $account);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}