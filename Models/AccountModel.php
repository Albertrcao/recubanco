<?php
require_once('../Helpers/DBManager.php');
require_once('../Models/Account.php');

use DBManager;

function getLastId()
{
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM cuenta ORDER BY id DESC limit 1";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->closeConnection();
        if (sizeof($rt) > 0) {
            return $rt[0]['id'];
        } else {
            return 0;
        }


    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function getUserId($dni)
{
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM cliente WHERE dni=:dni";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':dni', $dni);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rt[0]['id'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function createAccount($user)
{

    $manager = new DBManager();
    $rt = null;
    try {
        $lastId = getLastId() + 1;
        $len = strlen($lastId);
        $iban = '';
        for ($i = 1; $i < 24 - $len; $i++) {
            $iban .= '0';
        }
        $iban .= $lastId;
        $id_cliente = getUserId($user['dni']);
        $sql = "INSERT INTO cuenta (cuenta,id_cliente,saldo,creacion) VALUES (:cuenta,:id_cliente,0,now())";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':cuenta', $iban);
        $stmt->bindParam(':id_cliente', $id_cliente);
        $stmt->execute();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }


}

function getSalary($account)
{
    $manager = new DBManager();
    try {
        $sql = "SELECT saldo FROM cuenta WHERE id=:id";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $account);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rt[0]['salary'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getAccounts($dni){
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM cuenta WHERE id_cliente=:id_cliente";
        $stmt = $manager->getConnection()->prepare($sql);
        $id_cliente=getUserId($dni);
        $stmt->bindParam(':id_cliente',$id_cliente);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->closeConnection();
        return $rt;


    }catch(PDOException $e){
        echo $e->getMessage();
    }
}

function accountExisting($account){
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM cuenta WHERE id=:id";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':id',$account);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($rt)>0){
            return true;
        }else{
            return false;
        }

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

